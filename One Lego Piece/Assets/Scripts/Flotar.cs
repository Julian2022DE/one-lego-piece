using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flotar : MonoBehaviour
{
    #region variables
    [SerializeField] private float frecuencia;
    [SerializeField] private float magnitud;
    [SerializeField] private Vector3 Y_Move;
    private Vector3 Position_pj;
    #endregion

    private void Start()
    {
        Position_pj = transform.localPosition;
    }

    private void Update()
    {
        transform.localPosition = (Position_pj + transform.up * (Mathf.Sin(Time.time * frecuencia) * magnitud) + Y_Move);
    }
}
