using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{
    public AudioClip coinsaudio;
    private void OnTriggerEnter(Collider collision)

    {
        if (collision.gameObject.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(coinsaudio, gameObject.transform.position);
            if (coinsaudio == true)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
