using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class score : MonoBehaviour
{
    private int Score;
    public TextMeshProUGUI scoreText;
    // Start is called before the first frame update
    void Start()
    {
        Score = 0;
    }
    private void OnTriggerEnter(Collider collision)
      {
        if (collision.gameObject.tag == "Moneda")
        {
            Score++;
            scoreText.text = "Score = " + "" + Score;
        }
    }
}
