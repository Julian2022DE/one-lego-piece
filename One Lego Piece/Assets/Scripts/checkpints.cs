using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkpints : MonoBehaviour
{
    [SerializeField] GameObject player;
    [SerializeField] List<GameObject> checkpoints;
    [SerializeField] Vector3 vectorpoint;
    [SerializeField] float dead;

    private void Update()
    {
        if(player.transform.position.y < -dead)
        {
            player.transform.position = vectorpoint;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        vectorpoint = player.transform.position;
        Destroy(other.gameObject);
    }
}
