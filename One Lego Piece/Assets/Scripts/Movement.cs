using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public Animator anim;
    [Range(0, 10)]

    Rigidbody rb;

    bool Jumpfloor = false;
    bool JumpTrue = false;
    public float jumpoforce = 5f;

    public float movementspeed;
    public float rotationspeed;

    bool golpear = false;

    public AudioClip runAudio;
    public AudioClip jumpAuido;
    public AudioClip golpeoAudio;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void Update()
    {
       
        Vector3 moveDireccion = new Vector3(0, 0, Input.GetAxis("Vertical"));
        anim.SetFloat("speed", moveDireccion.z);
        transform.Translate(moveDireccion * movementspeed * Time.deltaTime);
        transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * rotationspeed,0));
        
        if (Input.GetButtonDown("Shift"))
        {
            movementspeed = 20f;
            anim.SetBool("Run", true);
            AudioSource.PlayClipAtPoint(runAudio, gameObject.transform.position);
        }
        if(Input.GetButtonUp("Shift"))
        {
            movementspeed = 10f;
            anim.SetBool("Run", false);
            AudioSource.PlayClipAtPoint(runAudio, gameObject.transform.position);
        }

     

        Vector3 floor = transform.TransformDirection(Vector3.down);

        if(Physics.Raycast(transform.position, floor, 1f))
        {
            Jumpfloor = true;   
        }
        else
        {
            Jumpfloor = false;
        }
        JumpTrue = Input.GetButtonDown("Jump");

        if (JumpTrue && Jumpfloor)
        {
            AudioSource.PlayClipAtPoint(jumpAuido, gameObject.transform.position);
            rb.AddForce(new Vector3(0, jumpoforce, 0),ForceMode.Impulse);
            anim.SetBool("Jump", true);
        }

        if (!JumpTrue && Jumpfloor)
        {
            anim.SetBool("Jump", false);
        }

        golpear = Input.GetMouseButtonDown(0);

        if(golpear)
        {
            AudioSource.PlayClipAtPoint(golpeoAudio, gameObject.transform.position);
            anim.SetBool("Golpear", true);
        }

        if(!golpear)
        {
            anim.SetBool("Golpear", false);
        }
    }


}
